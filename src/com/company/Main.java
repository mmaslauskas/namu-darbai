package com.company;

import com.Student;
import com.StudentFactory;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        DisplayStudents();
    }

    public static void DisplayStudents()
    {
        Scanner reader = new Scanner(System.in);
        String info;

        System.out.println("Type exit to leave");
        System.out.println("Input pattern:");
        System.out.println("firstName lastName degree subject average");

        while(true)
        {
            info = reader.nextLine();

            if (info.equalsIgnoreCase("Exit"))
                break;

            String[] studentDetails = info.split(" ");

            String firstName = studentDetails[0];
            String lastName = studentDetails[1];
            String degree = studentDetails[2];
            String subject = studentDetails[3];
            double average = Double.parseDouble(studentDetails[4]);

            Student student = StudentFactory.GetStudent(firstName, lastName, degree, subject, average);

            System.out.println(student.GetName() + " gaus " + student.CountScholarship() + " € stipendija");
        }
    }
}
