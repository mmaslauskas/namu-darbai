package com;

public class Undergraduate extends Student
{
    public Undergraduate(String firstName, String lastName, String degree, String subject, double average)
    {
        super(firstName, lastName, degree, subject, average);
    }

    @Override
    public int CountScholarship()
    {
        if (average > 8) return 100;
        
        else return 0;
    }
}
