package com;

public class PhD extends Student
{
    public PhD(String firstName, String lastName, String degree, String subject, double average)
    {
        super(firstName, lastName, degree, subject, average);
    }

    @Override
    public String GetName()
    {
        return "Dr. " + super.GetName();
    }

    @Override
    public int CountScholarship()
    {
        return 800;
    }
}
