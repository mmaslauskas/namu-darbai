package com;

public abstract class Student
{
    protected final String firstName;
    protected final String lastName;
    protected final String degree;
    protected final String subject;
    protected final double average;

    public Student(String firstName, String lastName, String degree, String subject, double average)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.degree = degree;
        this.subject = subject;
        this.average = average;
    }

    public String GetName()
    {
        return firstName + " " + lastName;
    }

    public abstract int CountScholarship();
}
