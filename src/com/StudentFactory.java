package com;

public class StudentFactory
{
    public static Student GetStudent(String firstName, String lastName, String degree, String subject, double average)
    {
        if (degree.equalsIgnoreCase("Undergraduate"))
            return new Undergraduate(firstName, lastName, degree, subject, average);

        else if (degree.equalsIgnoreCase("Postgraduate"))
            return new Postgraduate(firstName, lastName, degree, subject, average);

        else if (degree.equalsIgnoreCase("PhD"))
            return new PhD(firstName, lastName, degree, subject, average);

        throw new IllegalArgumentException("Non - existant degree: " + degree);

    }
}
