package com;

import java.util.Arrays;

public class Postgraduate extends Student
{
    public Postgraduate(String firstName, String lastName, String degree, String subject, double average)
    {
        super(firstName, lastName, degree, subject, average);
    }

    @Override
    public int CountScholarship()
    {
        String[] exactSciences = {"Mathematics", "Physics", "Chemistry", "Engineering", "ComputerScience"};
        boolean result = Arrays.stream(exactSciences).anyMatch(subject::equals);

        if (result) return 200;
        else return 0;
    }

}
